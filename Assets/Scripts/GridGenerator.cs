﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class GridGenerator : MonoBehaviour {

    public GameObject tilePrefab;
    public int numberOfTiles = 25;
    public int tilesPerRow = 5;
    public float distanceBetweenTiles = 1.0f;

    public List<Vector3> tileNodes = new List<Vector3>();
    void Awake()
    {
        InitialiseGrid();
    }

	// Use this for initialization
	void Start ()
    {
        
        DrawGrid();
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void InitialiseGrid()
    {
        float xOffset = 0.0f;
        float zOffset = 0.0f;
        for (int i = 0; i < numberOfTiles; i++)
        {
            xOffset += distanceBetweenTiles;
            if (i % tilesPerRow == 0)
            {
                zOffset += distanceBetweenTiles;
                xOffset = 0;
            }
            tileNodes.Add(new Vector3(transform.position.x + xOffset, transform.position.y, transform.position.z + zOffset));
        }

    }

    void DrawGrid()
    {
        foreach(Vector3 vec in tileNodes)
        {
            Instantiate(tilePrefab, vec, transform.rotation);
        }
    }
}
