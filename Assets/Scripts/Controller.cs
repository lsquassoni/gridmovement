﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Controller : MonoBehaviour {

    List<Vector3> positions;
    int posIndex = 0;

    GridGenerator gg;

    // Use this for initialization
    void Start()
    {
        gg = GameObject.Find("Grid").GetComponent<GridGenerator>();
        positions = gg.tileNodes;
        transform.position = positions[posIndex];
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMovement();
    }

    void PlayerMovement()
    {
        if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            if(posIndex != positions.Count - 1)
                posIndex++;

            transform.position = positions[posIndex];
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (posIndex > 0)
                posIndex--;

            transform.position = positions[posIndex];
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (posIndex != positions.Count - 1)
                posIndex += gg.tilesPerRow;

            transform.position = positions[posIndex];
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (posIndex > 0)
                posIndex -= gg.tilesPerRow;

            transform.position = positions[posIndex];
        }
    }
}
